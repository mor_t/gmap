/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yournamehere.shared;

import com.google.gwt.user.client.rpc.RemoteService;

/**
 *
 * @author Mor
 */
public interface ButtonPressData extends RemoteService
{
    public String getData();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yournamehere.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.maps.client.MapOptions;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.control.ControlAnchor;
import com.google.gwt.maps.client.control.ControlPosition;
import com.google.gwt.maps.client.control.LargeMapControl3D;
import com.google.gwt.maps.client.control.MapTypeControl;
import com.google.gwt.maps.client.control.MenuMapTypeControl;
import com.google.gwt.maps.client.geom.LatLng;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Main entry point.
 *
 * @author Mor
 */
public class MainEntryPoint implements EntryPoint {

    private RootPanel rootPanel;
    /**
     * Creates a new instance of MainEntryPoint
     */
    public MainEntryPoint() {
    }

    /**
     * The entry point method, called automatically by loading a module that
     * declares an implementing class as an entry-point
     */
    @Override
    public void onModuleLoad() {
                rootPanel = RootPanel.get("rootdiv");
		rootPanel.setWidth("400px");
		rootPanel.setHeight("400px");

		MapWidget mapWidget = new MapWidget();
		mapWidget.setWidth("400px");
		mapWidget.setHeight("400px");
                mapWidget.addControl(new MenuMapTypeControl(), new ControlPosition(ControlAnchor.TOP_RIGHT, 0, 0));
                
                mapWidget.addControl(new MapButton(new ControlPosition(ControlAnchor.TOP_LEFT, 0, 0)));

		rootPanel.add(mapWidget);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yournamehere.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.control.Control.CustomControl;
import com.google.gwt.maps.client.control.ControlPosition;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;
import org.yournamehere.shared.ButtonPressData;
import org.yournamehere.shared.ButtonPressDataAsync;
/**
 *
 * @author Mor
 */
public class MapButton extends CustomControl{
    private final ButtonPressDataAsync service;
    private Button btn = new Button("ok");
    public MapButton(ControlPosition defaultPosition)
    {
        super(defaultPosition);
        service = (ButtonPressDataAsync) GWT.create(ButtonPressData.class);
        ServiceDefTarget endpoint = (ServiceDefTarget) service;
        endpoint.setServiceEntryPoint(GWT.getModuleBaseURL() + "data");        

        btn.addClickHandler(new ButtonClickListener());
    }
    @Override
    public boolean isSelectable() {
        return true;
    }

    @Override
    protected Widget initialize(MapWidget map) {
        return btn;
    }
     private class ButtonClickListener implements ClickHandler {


        @Override
        public void onClick(ClickEvent event) {
            service.getData(new AsyncCallback() {

                @Override
                public void onFailure(Throwable e) {
                    GWT.log("Error:" + e.toString());
                }
                @Override
                public void onSuccess(Object obj) {
                    if (obj != null) {
                        DialogBox dlg = new DialogBox(false, false);
                        dlg.center();
                        dlg.setText(obj.toString());
                        dlg.show();
                    } else {
                        GWT.log("Server call returned nothing");
                    }
                }
            });
        }
    }
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.yournamehere.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import org.yournamehere.shared.ButtonPressData;


public class ButtonPressDataImpl extends RemoteServiceServlet implements ButtonPressData {

    @Override
    public String getData()
    {
        return "Hello from server";
    }
    
}
